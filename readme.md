Segue links das chamadas

Usuario:

Cadastrar Usuário:
POST localhost:8080/usuario/
Exemplo:
{
     "nome": "Rafael",
     "cpf": "15036184041",
    "email": "rafael@hotmail.com""
}

Atualizar Usuário:
PUT localhost:8080/usuario/{id}

Buscar Usuario por ID:
GET localhost:8080/usuario/{id}

Buscar todos usuários
GET localhost:8080/usuario/

--------------------------------------------
Batida de Ponto

Cadastrar ponto
POST localhost:8080/ponto
Exemplo:
{
    "idUsuario": 4,
    "data": "2020-06-02T05:50:00",
    "tipoBatida": "ENTRADA"

}

Buscar ponto por usuário
GET localhost:8080/usuario/{id}

Tipo de batidas
ENTRADA,
SAIDA

