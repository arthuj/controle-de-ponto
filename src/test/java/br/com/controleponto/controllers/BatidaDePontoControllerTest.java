package br.com.controleponto.controllers;

import br.com.controleponto.enums.TipoBatida;
import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.BatidaDePontoEntradaDTO;
import br.com.controleponto.models.dtos.BatidaDePontoRespostaDTO;
import br.com.controleponto.services.BatidaDePontoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(BatidaDePontoController.class)
public class BatidaDePontoControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    BatidaDePontoService batidaDePontoService;

    private BatidaDePontoEntradaDTO batidaDePontoEntradaDTO;
    private Usuario usuario;
    private BatidaDePonto batidaDePonto;
    private BatidaDePontoRespostaDTO batidaDePontoRespostaDTO;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        batidaDePonto = new BatidaDePonto();
        batidaDePontoEntradaDTO = new BatidaDePontoEntradaDTO();
        //batidaDePontoEntradaDTO = new BatidaDePontoEntradaDTO(1, LocalDateTime.now(), TipoBatida.ENTRADA);
        batidaDePontoEntradaDTO.setTipoBatida(TipoBatida.ENTRADA);
        batidaDePontoEntradaDTO.setIdUsuario(1);
        batidaDePontoRespostaDTO = new BatidaDePontoRespostaDTO();
        batidaDePonto.setId(1);
        batidaDePonto.setTipoBatida(batidaDePontoEntradaDTO.getTipoBatida());
    }

    @Test
    public void testarCadastrarBatidaDePonto() throws Exception{
        Mockito.when(batidaDePontoService.criarBatidaDePonto(Mockito.any(BatidaDePontoEntradaDTO.class)))
                .then(batidaDePontoObjeto -> {
            batidaDePonto.setId(1);
            return batidaDePonto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(batidaDePonto);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarTodosUsuarios() throws Exception{
        Mockito.when(batidaDePontoService.listarTodasBatidasPorUsuario(Mockito.anyInt()))
                .then(batidaDePontoRespostaDTOObjeto -> {
                    batidaDePontoRespostaDTO.setTotalDeHoras(1);
                    return batidaDePontoRespostaDTO;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto= mapper.writeValueAsString(batidaDePontoRespostaDTO);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/ponto/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalDeHoras", CoreMatchers.equalTo(1)));
    }

}
