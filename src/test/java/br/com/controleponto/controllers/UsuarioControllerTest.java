package br.com.controleponto.controllers;

import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.UsuarioEntradaDTO;
import br.com.controleponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    UsuarioService usuarioService;

    @Autowired
    MockMvc mockMvc;

    private  Usuario usuario;
    private UsuarioEntradaDTO usuarioEntradaDTO;
    @BeforeEach
    public void setUp()
    {
        usuario = new Usuario();
        usuarioEntradaDTO = new UsuarioEntradaDTO();
        usuario.setId(1);
        usuario.setNome("Rafael");
        usuario.setCpf("15036184041");
        usuario.setEmail("rafael@hotmail.com");
        usuarioEntradaDTO.setNome("Joao");
        usuarioEntradaDTO.setCpf(usuario.getCpf());
        usuarioEntradaDTO.setEmail(usuario.getEmail());

    }

    @Test
    public void testarCadastrarUsuario() throws Exception {
        Mockito.when(usuarioService.cadastrarUsuario(Mockito.any(UsuarioEntradaDTO.class))).then(usuarioObjeto -> {
            usuario.setId(1);
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario= mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarAtualizarUsuario() throws Exception {
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyInt(),Mockito.any(UsuarioEntradaDTO.class))).then(usuarioObjeto -> {
            usuario.setId(1);
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario= mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarUsuarioPorId() throws Exception{
        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).then(usuarioObjeto -> {
            usuario.setId(1);
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario= mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarTodosUsuarios() throws Exception{
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioService.buscarTodosUsuariosCadastrados()).thenReturn((List<Usuario>) usuarios);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/usuario")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }


}
