package br.com.controleponto.services;

import br.com.controleponto.enums.TipoBatida;
import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.BatidaDePontoEntradaDTO;
import br.com.controleponto.models.dtos.BatidaDePontoRespostaDTO;
import br.com.controleponto.repositories.BatidaDePontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class BatidaDePontoTest {

    @MockBean
    BatidaDePontoRepository batidaDePontoRepository;

    @MockBean
    UsuarioService usuarioService;


    @Autowired
    BatidaDePontoService batidaDePontoService;

    private BatidaDePonto batidaDePonto;
    private BatidaDePontoEntradaDTO batidaDePontoEntradaDTO;
    private BatidaDePontoRespostaDTO batidaDePontoRespostaDTO;
    private Usuario usuario;
    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        batidaDePonto = new BatidaDePonto();
        batidaDePontoEntradaDTO = new BatidaDePontoEntradaDTO(1, LocalDateTime.now(), TipoBatida.ENTRADA);
        batidaDePontoRespostaDTO = new BatidaDePontoRespostaDTO();
        batidaDePonto.setId(1);
        batidaDePonto.setData(batidaDePontoEntradaDTO.getData());
        batidaDePonto.setTipoBatida(batidaDePontoEntradaDTO.getTipoBatida());
    }

    @Test
    public void testarCriarBatidaPonto()
    {
        Mockito.when(usuarioService.buscarUsuarioPorID(batidaDePontoEntradaDTO.getIdUsuario())).thenReturn(usuario);
        batidaDePonto.setUsuario(usuario);
        batidaDePontoService.criarBatidaDePonto(batidaDePontoEntradaDTO);
        Mockito.verify(batidaDePontoRepository).save(any(BatidaDePonto.class));
    }

//    @Test
//    public void testarListarTodasBatidasPorUsuario()
//    {
//        Iterable<BatidaDePonto> batidaDePontos = Arrays.asList();
//        Iterable<BatidaDePonto> batidaDePontosEntrada = Arrays.asList();
//        Iterable<BatidaDePonto> batidaDePontosSaida = Arrays.asList();
//        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).thenReturn(usuario);
//        Mockito.when(batidaDePontoRepository.findAllByUsuarioOrderByDataAsc(Mockito.any(Usuario.class)))
//                .thenReturn((List<BatidaDePonto>)batidaDePontos);
//        Mockito.when(batidaDePontoRepository.findAllByUsuarioAndTipoBatidaOrderByDataAsc(Mockito.any(Usuario.class),Mockito.any(TipoBatida.class)))
//                .thenReturn((List<BatidaDePonto>)batidaDePontosEntrada);
//        Mockito.when(batidaDePontoRepository.findAllByUsuarioAndTipoBatidaOrderByDataAsc(Mockito.any(Usuario.class),Mockito.any(TipoBatida.class)))
//                .thenReturn((List<BatidaDePonto>)batidaDePontosSaida);
//        batidaDePontoService.listarTodasBatidasPorUsuario(Mockito.anyInt());
//        verify(batidaDePontoRepository).findAllByUsuarioOrderByDataAsc(Mockito.any(Usuario.class));
//
//    }
}
