package br.com.controleponto.services;

import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.UsuarioEntradaDTO;
import br.com.controleponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    UsuarioService usuarioService;

    @MockBean
    UsuarioRepository usuarioRepository;

    private Usuario usuario;
    private UsuarioEntradaDTO usuarioEntradaDTO;

    @BeforeEach
    public void setUp()
    {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Arthur");
        usuario.setCpf("36158006050");
        usuario.setEmail("arthur@hotmail.com");
        usuario.setDataCadastro(LocalDateTime.now());
        usuarioEntradaDTO = new UsuarioEntradaDTO();
        usuarioEntradaDTO.setNome(usuario.getNome());
        usuarioEntradaDTO.setCpf(usuario.getCpf());
        usuarioEntradaDTO.setEmail(usuario.getEmail());

    }

    @Test
    public void testarCadastrarUsuario()
    {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        Usuario usuarioTeste = usuarioService.cadastrarUsuario(usuarioEntradaDTO);

        Assertions.assertEquals(usuarioTeste.getNome(),"Arthur");
        Mockito.verify(usuarioRepository).save(any(Usuario.class));

    }

    @Test
    public void testarAtualizarUsuario(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario usuarioTeste = usuarioService.atualizarUsuario(usuario.getId(),usuarioEntradaDTO);

        Mockito.verify(usuarioRepository).save(any(Usuario.class));

    }

    @Test
    public void testarBuscarUsuarioPorId()
    {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(java.util.Optional.ofNullable(usuario));
        Usuario usuarioTeste = usuarioService.buscarUsuarioPorID(usuario.getId());

        Assertions.assertEquals(usuario,usuarioTeste);
    }

    @Test
    public void testarBuscarUsuarioPorIdNaoEncontrado(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarUsuarioPorID(usuario.getId());});
    }

    @Test
    public void testarBuscarTodosUsuarios()
    {
        Iterable<Usuario> usuarios = Arrays.asList();
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);
        Iterable<Usuario> usuariosTeste = usuarioService.buscarTodosUsuariosCadastrados();

        Assertions.assertEquals(usuarios,usuariosTeste);
    }
}
