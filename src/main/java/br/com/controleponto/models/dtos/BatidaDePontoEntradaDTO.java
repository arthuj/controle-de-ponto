package br.com.controleponto.models.dtos;

import br.com.controleponto.enums.TipoBatida;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class BatidaDePontoEntradaDTO {

    @NotNull
    private int idUsuario;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime data;
    @NotNull
    private TipoBatida tipoBatida;

    public BatidaDePontoEntradaDTO(@NotNull int idUsuario, LocalDateTime data, @NotNull TipoBatida tipoBatida) {
        this.idUsuario = idUsuario;
        this.data = data;
        this.tipoBatida = tipoBatida;
    }

    public BatidaDePontoEntradaDTO() {
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public TipoBatida getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatida tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
