package br.com.controleponto.models.dtos;

import br.com.controleponto.models.BatidaDePonto;

import java.time.LocalDateTime;

public class BatidaDePontoRespostaDTO {

    Iterable<BatidaDePonto> batidaDePontos;
    long totalDeHoras;

    public BatidaDePontoRespostaDTO(Iterable<BatidaDePonto> batidaDePontos, long totalDeHoras) {
        this.batidaDePontos = batidaDePontos;
        this.totalDeHoras = totalDeHoras;
    }

    public BatidaDePontoRespostaDTO() {
    }

    public Iterable<BatidaDePonto> getBatidaDePontos() {
        return batidaDePontos;
    }

    public void setBatidaDePontos(Iterable<BatidaDePonto> batidaDePontos) {
        this.batidaDePontos = batidaDePontos;
    }

    public long getTotalDeHoras() {
        return totalDeHoras;
    }

    public void setTotalDeHoras(long totalDeHoras) {
        this.totalDeHoras = totalDeHoras;
    }
}
