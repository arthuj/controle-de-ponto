package br.com.controleponto.models.dtos;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class UsuarioEntradaDTO {

    @NotBlank
    private String nome;
    @CPF
    private String cpf;
    @Email
    private String email;

    public UsuarioEntradaDTO() {
    }

    public UsuarioEntradaDTO(@NotBlank String nome, @CPF String cpf, @Email String email) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
