package br.com.controleponto.models;

import br.com.controleponto.enums.TipoBatida;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class BatidaDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Usuario usuario;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDateTime data;
    @NotNull
    private TipoBatida tipoBatida;

    public BatidaDePonto() {
    }

    public BatidaDePonto(Usuario usuario, LocalDateTime data, @NotNull TipoBatida tipoBatida) {
        this.usuario = usuario;
        this.data = data;
        this.tipoBatida = tipoBatida;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public TipoBatida getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatida tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
