package br.com.controleponto.repositories;

import br.com.controleponto.enums.TipoBatida;
import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface BatidaDePontoRepository extends CrudRepository<BatidaDePonto,Integer> {

    Iterable<BatidaDePonto> findAllByUsuarioOrderByDataAsc(Usuario usuario);

    Iterable<BatidaDePonto> findAllByUsuarioAndTipoBatidaOrderByDataAsc(Usuario usuario, TipoBatida tipoBatida);
}
