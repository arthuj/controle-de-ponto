package br.com.controleponto.controllers;

import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.UsuarioEntradaDTO;
import br.com.controleponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Valid UsuarioEntradaDTO usuarioEntradaDTO){
        try {
            Usuario usuarioObjeto = usuarioService.cadastrarUsuario(usuarioEntradaDTO);
            return usuarioObjeto;
        }
        catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,ex.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Usuario atualizarUsuario(@PathVariable(name = "id") int id, @RequestBody @Valid UsuarioEntradaDTO usuarioEntradaDTO) {
        try {
            Usuario usuario = usuarioService.atualizarUsuario(id, usuarioEntradaDTO);
            return usuario;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping
    public Iterable<Usuario> buscarTodosUsuariosCadastrados(){
        Iterable<Usuario> usuarios = usuarioService.buscarTodosUsuariosCadastrados();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Usuario buscarUsuarioPorId(@PathVariable(name = "id") int id){
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorID(id);
            return usuario;
        }
        catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }


}
