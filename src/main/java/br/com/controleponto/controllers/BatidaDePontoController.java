package br.com.controleponto.controllers;


import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.dtos.BatidaDePontoEntradaDTO;
import br.com.controleponto.models.dtos.BatidaDePontoRespostaDTO;
import br.com.controleponto.services.BatidaDePontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/ponto")
public class BatidaDePontoController {

    @Autowired
    BatidaDePontoService batidaDePontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaDePonto cadastrarBatidaDePonto(@RequestBody @Valid BatidaDePontoEntradaDTO batidaDePonto)
    {
        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.criarBatidaDePonto(batidaDePonto);
        return  batidaDePontoObjeto;
    }

    @GetMapping("/{id}")
    public BatidaDePontoRespostaDTO listarBatidasPorUsuario(@PathVariable(name = "id") int id)
    {
        try {
            BatidaDePontoRespostaDTO batidaDePontoRespostaDTO = batidaDePontoService.listarTodasBatidasPorUsuario(id);
            return batidaDePontoRespostaDTO;
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
