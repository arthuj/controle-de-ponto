package br.com.controleponto.services;

import br.com.controleponto.enums.TipoBatida;
import br.com.controleponto.models.BatidaDePonto;
import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.BatidaDePontoEntradaDTO;
import br.com.controleponto.models.dtos.BatidaDePontoRespostaDTO;
import br.com.controleponto.repositories.BatidaDePontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;

@Service
public class BatidaDePontoService {

    @Autowired
    BatidaDePontoRepository batidaDePontoRepository;

    @Autowired
    UsuarioService usuarioService;

    public BatidaDePonto criarBatidaDePonto(BatidaDePontoEntradaDTO batidaDTO){

        Usuario usuario = usuarioService.buscarUsuarioPorID(batidaDTO.getIdUsuario());
        BatidaDePonto batidaDePonto = new BatidaDePonto(usuario, batidaDTO.getData(),batidaDTO.getTipoBatida());
        batidaDePonto = batidaDePontoRepository.save(batidaDePonto);
        return batidaDePonto;
    }

    public BatidaDePontoRespostaDTO listarTodasBatidasPorUsuario(int id){
        Usuario usuario = usuarioService.buscarUsuarioPorID(id);
        Iterable<BatidaDePonto> iterablePorUsuario = batidaDePontoRepository.findAllByUsuarioOrderByDataAsc(usuario);
        Iterable<BatidaDePonto> iterableBatidaEntrada = batidaDePontoRepository.findAllByUsuarioAndTipoBatidaOrderByDataAsc(usuario,TipoBatida.ENTRADA);
        Iterable<BatidaDePonto> iterableBatidaSaida = batidaDePontoRepository.findAllByUsuarioAndTipoBatidaOrderByDataAsc(usuario,TipoBatida.SAIDA);
        long totalHoras = CalcularTotalHorasTrabalhadas((List<BatidaDePonto>)iterableBatidaEntrada, (List<BatidaDePonto>) iterableBatidaSaida);
        BatidaDePontoRespostaDTO batidaDePontoRespostaDTO = new BatidaDePontoRespostaDTO(iterablePorUsuario, totalHoras);
        return batidaDePontoRespostaDTO;
    }

    private long CalcularTotalHorasTrabalhadas(List<BatidaDePonto> listaBatidaDePontoEntrada,
                                              List<BatidaDePonto> listaBatidaDePontoSaida){
        long totalHoras = 0;
        if(listaBatidaDePontoEntrada.size() > 0 && listaBatidaDePontoSaida.size() > 0) {
            for (int i = 0; listaBatidaDePontoEntrada.size() > i && listaBatidaDePontoSaida.size() > i; i++)
            {
                long diffHoras = Duration.between(listaBatidaDePontoEntrada.get(i).getData(),
                        listaBatidaDePontoSaida.get(i).getData()).toHours();
                totalHoras += diffHoras;
            }
            return totalHoras;
        }
        else
            throw  new RuntimeException("Não existe batidas de entrada e saída até o momento");
    }
}
