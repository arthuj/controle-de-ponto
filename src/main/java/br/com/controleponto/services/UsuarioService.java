package br.com.controleponto.services;

import br.com.controleponto.models.Usuario;
import br.com.controleponto.models.dtos.UsuarioEntradaDTO;
import br.com.controleponto.repositories.UsuarioRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario(UsuarioEntradaDTO usuarioEntradaDTO){
        Usuario usuario = new Usuario();
        usuario.setNome(usuarioEntradaDTO.getNome());
        usuario.setCpf(usuarioEntradaDTO.getCpf());
        usuario.setEmail(usuarioEntradaDTO.getEmail());
        usuario.setDataCadastro(LocalDateTime.now());
        usuario = usuarioRepository.save(usuario);
        return usuario;
    }

    public Usuario atualizarUsuario(int id, UsuarioEntradaDTO usuarioEntradaDTO){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if(optionalUsuario.isPresent())
        {
            Usuario usuario = optionalUsuario.get();
            usuario.setNome(usuarioEntradaDTO.getNome());
            usuario.setCpf(usuarioEntradaDTO.getCpf());
            usuario.setEmail(usuarioEntradaDTO.getEmail());
            usuario = usuarioRepository.save(usuario);
            return usuario;
        }
        else
            throw new RuntimeException("Usuário não encontrado.");
    }

    public Usuario buscarUsuarioPorID(int id){
        Optional<Usuario> usuario = usuarioRepository.findById(id);
        if(usuario.isPresent())
        {
            return usuario.get();
        }
        else
            throw new RuntimeException("Usuário não encontrado.");
    }

    public Iterable<Usuario> buscarTodosUsuariosCadastrados(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }
}
