package br.com.controleponto.enums;

public enum TipoBatida {
    ENTRADA,
    SAIDA
}
